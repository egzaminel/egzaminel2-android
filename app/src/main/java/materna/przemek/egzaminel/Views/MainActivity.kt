package materna.przemek.egzaminel.Views

import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.LinearLayout
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import materna.przemek.egzaminel.Adapters.RecyclerListAdapter
import materna.przemek.egzaminel.Adapters.SimpleButton
import materna.przemek.egzaminel.Model.Group
import materna.przemek.egzaminel.Network.EgzaminelAPIService
import materna.przemek.egzaminel.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val result : Call<List<Group>> = EgzaminelAPIService.service.getUserInfo("2")
        result.enqueue(object : Callback<List<Group>> {
            override fun onResponse(call: Call<List<Group>>?, response: Response<List<Group>>?) {
                Log.i("XD", "XD")
            }

            override fun onFailure(call: Call<List<Group>>?, t: Throwable?) {
                t?.printStackTrace()
            }
        })

        // Buttons...
        val b0 = SimpleButton(0,
                "Button1",
                Color.BLACK,
                null,
                Color.WHITE,
                { _ -> ( Toast.makeText(applicationContext, title, Toast.LENGTH_LONG)).show() }
        )
        val b1 = SimpleButton(0,
                "Button1",
                Color.BLACK,
                null,
                Color.RED,
                { _ -> ( Toast.makeText(applicationContext, title, Toast.LENGTH_LONG)).show() }
        )
        val b2 = SimpleButton(0,
                "Button1",
                Color.BLACK,
                null,
                Color.GREEN,
                { _ -> ( Toast.makeText(applicationContext, title, Toast.LENGTH_LONG)).show() }
        )
        val b3 = SimpleButton(0,
                "Button1",
                Color.BLACK,
                null,
                Color.YELLOW,
                { _ -> ( Toast.makeText(applicationContext, title, Toast.LENGTH_LONG)).show() }
        )

        val buttons = listOf(b0, b1, b2, b3)

        recycler_view.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        recycler_view.adapter = RecyclerListAdapter(buttons)
        recycler_view.adapter.notifyDataSetChanged()
    }
}
