package materna.przemek.egzaminel.Database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.ManagedSQLiteOpenHelper

/**
 * Created by przemyslaw.materna on 29.09.2017.
 */
class EgzaminelSqlHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "EgzaminelDatabase") {

    companion object {
        private var instance: EgzaminelSqlHelper? = null;

        @Synchronized
        fun getInstance(ctx: Context) : EgzaminelSqlHelper {
            if (instance == null) {
                instance = EgzaminelSqlHelper(ctx.applicationContext)
            }
            return instance!!
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
    }
}

val Context.database : EgzaminelSqlHelper
    get() = EgzaminelSqlHelper.getInstance(applicationContext)