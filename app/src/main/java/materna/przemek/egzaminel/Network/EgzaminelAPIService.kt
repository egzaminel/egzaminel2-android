package materna.przemek.egzaminel.Network

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import materna.przemek.egzaminel.Model.Group
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.concurrent.TimeUnit

/**
* Created by Przemek on 01.10.2017.
*/
interface EgzaminelAPIService {
    companion object {
        private val gson : Gson = GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create()
        private val gsonConventerFactory : GsonConverterFactory = GsonConverterFactory.create(gson)
        private val client : OkHttpClient = OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .build()
        private val retrofit : Retrofit = Retrofit.Builder()
                .baseUrl(LocalConfigEmulator.baseURL)
                .client(client)
                .addConverterFactory(gsonConventerFactory)
                .build()

        val service : EgzaminelAPIService = EgzaminelAPIService.retrofit.create(EgzaminelAPIService::class.java)
    }

    @GET("user_data.php")
    fun getUserInfo(@Query("user_id") user_id: String) : Call<List<Group>>

}