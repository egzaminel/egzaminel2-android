package materna.przemek.egzaminel.Adapters

import android.graphics.Color
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import materna.przemek.egzaminel.R

/**
* Created by przemyslaw.materna on 29.09.2017.
*/
class RecyclerListAdapter(val buttons: List<AbstractButton>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        const val ERROR_CODE = -1
        const val BUTTON_VIEW_HOLDER = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : RecyclerView.ViewHolder? {
        return when (viewType) {
            BUTTON_VIEW_HOLDER -> ButtonViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.recycler_view_button, parent, false))
            else -> {
                null
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (buttons[position]) {
            is SimpleButton -> BUTTON_VIEW_HOLDER
            else -> {
                ERROR_CODE
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        if (position == ERROR_CODE) {
            Log.e("RecyclerListAdapter: ", "WRONG VIEW TYPE")
            return
        }

        when (holder) {
            is ButtonViewHolder -> holder.bindItems(buttons[position] as SimpleButton)
        }
    }

    override fun getItemCount(): Int {
        return buttons.size
    }

    class ButtonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(data: SimpleButton) {
            val iconIV = itemView.findViewById<ImageView>(R.id.icon)
            val titleTV = itemView.findViewById<TextView>(R.id.title)

            if (data.image != null) {
                iconIV.setImageDrawable(data.image)
            }

            titleTV.text = data.title
            titleTV.setTextColor(data.titleColor)
            itemView.setBackgroundColor(data.background)
            itemView.setOnClickListener(data.onItemClickListener)
        }
    }
}

abstract class AbstractButton(id: Long = -1)
data class SimpleButton(val id: Long,
                        val title: String, val titleColor: Int,
                        val image: Drawable?, val background: Int,
                        val onItemClickListener: (view: View) -> Unit) : AbstractButton(id)