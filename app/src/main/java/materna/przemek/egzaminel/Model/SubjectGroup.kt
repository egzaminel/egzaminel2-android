package materna.przemek.egzaminel.Model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Przemek on 01.10.2017.
 */
data class SubjectGroup(
        @SerializedName("id") val id : Int,
        @SerializedName("subject_id") val subjectId : Int,
        @SerializedName("date") val date : Date?,
        @SerializedName("perioid") val period : Int?,
        @SerializedName("place") val place : String?,
        @SerializedName("teacher") val teacher : String?,
        @SerializedName("description") val description : String?,
        @SerializedName("last_update") val lastUpdate : Date,
        @SerializedName("events") val events : List<Event>?
)