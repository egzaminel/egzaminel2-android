package materna.przemek.egzaminel.Model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Przemek on 01.10.2017.
 */
data class Event (
        @SerializedName("id") val id : Int,
        @SerializedName("parent_id") val parentId : Int,
        @SerializedName("name") val subjectId : String,
        @SerializedName("description") val description : String?,
        @SerializedName("date") val date : Date?,
        @SerializedName("place") val place : String?,
        @SerializedName("last_update") val lastUpdate : Date
)