package materna.przemek.egzaminel.Model

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by Przemek on 01.10.2017.
 */
data class Subject(
        @SerializedName("id") val id : Int,
        @SerializedName("group_id") val groupId : Int,
        @SerializedName("name") val subjectId : String,
        @SerializedName("description") val description : String?,
        @SerializedName("last_update") val lastUpdate : Date,
        @SerializedName("events") val events : List<Event>?,
        @SerializedName("subject_groups") val subjectGroups : List<SubjectGroup>?
)